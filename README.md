#### 项目介绍
提供简历中提到的项目运行预览效果及源代码

#### 一、仿安徽农业大学官网前端界面
1. 开发工具：Hbuilder
2. 所用技术：Html、Css、Bootstrap
3. 主要功能：参考安徽农业大学官网的布局，对前端基础Html、Css的语法以及Bootstrap框架进行练习与巩固。
4. 效果展示：
![输入图片说明](image/1%E3%80%81Bootstrap%E5%AE%9E%E7%8E%B0%E7%AE%80%E6%98%93%E5%AE%89%E5%86%9C%E4%B8%BB%E9%A1%B5up.png)
![输入图片说明](image/1%E3%80%81Bootstrap%E5%AE%9E%E7%8E%B0%E7%AE%80%E6%98%93%E5%AE%89%E5%86%9C%E4%B8%BB%E9%A1%B5down.png)
